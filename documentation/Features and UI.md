# abas-dashboardadmin-groups-widget features and visible UI-components

The abas-dashboardadmin-groups-widget can be used to create or edit groups and allocate users and dashboards to them.

## Images

![Alt text](Example.jpg "Widget base view")

## UI-Components

| Component | Description | Position
| --- | --- | --- |
| AddGroup-Button | Button | Header
| Filter groups | Input box | Header
| Groupname | Displays the groupname, id, number of members and number of dashboards | Table
| Update-Button | Button | Table
| Delete-Button | Button | Table

## Features

| Component | Description | Input Type | 
| --- | --- | --- |
Filter users | Filters the table of groups by name and id. | String | 
| AddGroup-Button | Clicking the button opens the group editor with a new group. | Button |
Update-Button | Clicking the button opens the group editor for the indicated group. | Button | 

## Group Editor

![Alt text](ExampleAddGroup.jpg "Widget group editor")

| Title | Description | Input Type | Condition |
| --- | --- | --- | --- | 
| Cancel | Do not appyly any changes and return to base view. | Button | - |
| Submit | Apply changes to the group and return to base view. | Button | Form filed completely |
| GroupID | Enter group id. | String | Three or more characters |
| Group Name | Enter group name. | String | Three or more characters |
| Description | Enter a description for the group. | String | - |
| Members | Displays amount of allocated members. Click opens members editor. | String | - |
| Dashboards | Displays amount of allocated dashboards. Click opens dashboards editor. | String | - |

![Alt text](ExampleDashboards.jpg "Widget dashboard editor")

| Title | Description | Input Type |
| --- | --- | --- |
| Back | Apply the changes and return to group editor. | Button |
| Filter dashboards | Filters the table of dashboards by name and id. | String |
| Table | Displays dashboards with id and name. If checkbox checked the dashboard is added to the group. | Checkbox |

![Alt text](ExampleMembers.jpg "Widget groups view")

| Title | Description | Input Type |
| --- | --- | --- |
| Back | Apply the changes and return to group editor. | Button |
| Filter users | Filters the table of users by name and email. | String |
| Table | Displays users with name and email. If checkbox checked the user is added to the group. | Checkbox |